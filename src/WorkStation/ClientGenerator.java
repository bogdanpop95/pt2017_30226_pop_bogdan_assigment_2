package WorkStation;
import java.util.*;

public class ClientGenerator implements Runnable{
	public static int ID = 1;
	private int nrServers;
	private Service servers[];
	Random r = new Random();
	private WorkStation ws;
	
	public ClientGenerator(int nrServers, Service servers[],WorkStation ws) {
		ID = 1;
		this.ws = ws;
		this.nrServers = nrServers;
		this.servers = new Service[nrServers];
		for(int i = 0; i < nrServers; i++) {
			this.servers[i] = servers[i];
		}
	}
	
	private int minIndex() {
		int index = 0;
		try {
			int min = servers[0].lengthCoada();
			for (int i = 1; i < nrServers; i++) {
				if(servers[i].lengthCoada() < min) {
					min = servers[i].lengthCoada();
					index = i;
				}
			}
		} catch (InterruptedException e) {
			 System.out.println("Intrerupere minIndex");
			 System.out.println( e.toString());
		}
		return index;
	}
	
	private int maxEndTime() {
		int max = 0;
		try {
			for(int i = 0; i < nrServers; i++) {
				if (servers[i].getEndTime() > max)
					max = servers[i].getEndTime();
			}
		} catch (InterruptedException e) {
			 System.out.println("Intrerupere maxEndTime");
			 System.out.println( e.toString());
		}
		return max;
	}
	
	public int randArrivingTime() {
		int result = r.nextInt(ws.maxArrivingTime - ws.minArrivingTime) + ws.minArrivingTime;
		return result;
	}
	
	public int randServiceTime() {
		int result = r.nextInt(ws.maxServiceTime - ws.minServiceTime) + ws.minServiceTime;
		return result;
	}
	
	public void run() {
			try {
				int i, j, count, max = 0, time = 0, rand = 0;
				for (i = 1; i <= this.maxEndTime(); i++) {
					if (i <= ws.getSimulationInterval() && time <  i) {
						rand = randArrivingTime();
						time = rand + ws.getLastArrivingTime();
					}
					count = 0;
					for (j = 0; j < nrServers; j++) {
						count += servers[j].lengthCoada();
					}
					if (count > max) {
						max = count;
						ws.peakHour = ws.getActualTime();
					}
					ws.setActualTime(1);
					ws.sim.setTimp(ws.getActualTime());
					System.out.println("Time : " + ws.getActualTime());
					if (i < ws.getSimulationInterval() && i == time) {
						ws.setLastArrivingTime(rand);
						int min = minIndex();
						Client client= new Client(ID++, ws.getLastArrivingTime(), this.randServiceTime());
						ws.sim.createClient(min, ID - 1, servers[min].lengthCoada());
						servers[min].addClient(client);
						System.out.println("Client(" + client.getID() + ") adaugat la Server-ul " + Integer.toString(min + 1) + " ArrivalTime: " + client.getArrivalTime() + " ServiceTime : " + client.getserviceTime());
					}
					Thread.sleep(995);
				}
			} catch(InterruptedException e) {
				 System.out.println("Intrerupere run ClientGenerator");
				 System.out.println( e.toString());
			}
			ws.setActualTime(1);
	}
		
}
