package WorkStation;
import java.util.*;

public class Service implements Runnable{
	private Vector<Client> coada = new Vector<Client>();
	private String name;
	private int actual = 0;
	private int emptyQueue = 0;
	private int index;
	private int endTime;
	private WorkStation ws;
	
	public Service(String name, WorkStation ws, int index) {
		this.ws = ws;
		this.name = name;
		this.index = index;
		endTime = ws.getSimulationInterval();
		coada.clear();
	}
	
	public void run() {
		try {
			while (ws.getActualTime() <= endTime)  {
				Client c = removeClient();
				if (c == null)
					break;
				// tratam cazul in care mai exista clienti in coada chiar daca s-a ajuns la simulationTime (nu ii dam afara)
				if (c.getFinishTime() > endTime)
					this.setEndTime(c.getFinishTime());
				Thread.sleep(c.getserviceTime()* 1010);
				ws.sim.removeClient(c.getID());
				coada.remove(0);
				// updatam waiting time-ul primului ramas in coada(daca sunt mai multi, se transfera la urmatorul pas)
				if(!coada.isEmpty())
					coada.get(0).setWaitingTime(c.getArrivalTime() + c.getWaitingTime() - coada.get(0).getArrivalTime());
				System.out.println("Client(" + c.getID() + ") served by " + this.name + " for " + c.getserviceTime() + " | WaitingTime: " + c.getWaitingTime());
			}
		} catch (InterruptedException e) {
			 System.out.println("Intrerupere");
			 System.out.println( e.toString());
		}
		ws.sim.setFree(index);
	}
	
	public synchronized Client removeClient() throws InterruptedException {
		while (coada.isEmpty() && (ws.getActualTime() <= ws.simulationInterval)) {
			wait(998);
			ws.sim.setEmpty(index);
			if(actual != ws.getActualTime()) {
				actual = ws.getActualTime();
				emptyQueue++;
			}			
		}
		if(!coada.isEmpty()) {
			ws.sim.setBusy(index);
			Client c = coada.get(0);
			// la finalizare service: 1. Actualizam (avg)waitingTime;
			ws.avgWaitingTime += c.getWaitingTime();
			ws.avgServiceTime += c.getserviceTime();
			notifyAll();
			return c;
		}
		return null;
	}
	
	public synchronized void addClient(Client client) throws InterruptedException{
		coada.addElement(client);
		this.notify();
	}
	
	public synchronized int lengthCoada() throws InterruptedException {
		int size = coada.size();
		return size;
	}
	
	public synchronized int getEndTime() throws InterruptedException {
		return endTime;
	}
	
	public synchronized void setEndTime(int amount) throws InterruptedException {
		endTime = amount;
	}
	
	public int getEmptyQueueTime() {
		return emptyQueue;
	}
}
