package WorkStation;

public class Client {
	private int ID;
	private int arrivalTime;		// timpul de sosire raportat la timpul real
	private int serviceTime;
	private int waitingTime = 0;
	
	public Client(int ID, int arrivalTime, int serviceTime) {
		this.ID = ID;
		this.arrivalTime = arrivalTime;
		this.serviceTime = serviceTime;
		this.waitingTime += serviceTime;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getArrivalTime() {
		return arrivalTime;
	}
	
	public int getserviceTime() {
		return serviceTime;
	}
	
	public void setWaitingTime(int amount) {
		this.waitingTime += amount;
	}
	
	public int getWaitingTime() {
		return this.waitingTime;
	}
	
	public int getFinishTime() {
		return getArrivalTime() + getWaitingTime();
	}
}
