package WorkStation;
import GUI.Simulation;

public class WorkStation{
	// Input data:
	public int minArrivingTime;		
	public int maxArrivingTime;
	public int minServiceTime;
	public int maxServiceTime;
	public int nrServers;				
	public int simulationInterval = 0;		// 0-simulationInterval (in seconds)
	// WorkStation Data:
	public int nrClients;
	private int lastArrivingTime = 0;
	private int actualTime = 0;				// The real-time (in seconds)
	public int avgWaitingTime = 0;
	public int avgServiceTime = 0;
	public int emptyQueueTime = 0;
	public int peakHour = 0;
	
	private Service servers[];
	private ClientGenerator gen;
	private Thread threads[];
	private Thread producer;
	public Simulation sim;
	
	public WorkStation(int a, int b, int c, int d, int e, int f) {
		nrServers = a;
		minArrivingTime = b;
		maxArrivingTime = c;
		minServiceTime = d;
		maxServiceTime = e;
		simulationInterval = f;
	}
	
	public synchronized int getLastArrivingTime() {
		return lastArrivingTime;
	}
	
	public synchronized int getActualTime() {
		return actualTime;
	}
	
	public synchronized int getSimulationInterval() {
		return simulationInterval;
	}
	
	
	public synchronized void setLastArrivingTime(int time) {
		lastArrivingTime += time;
	}
	
	public synchronized void setActualTime(int time) {
		actualTime += time;
	}
	
	public Service[] getServers() {
		return servers;
	}
	
	public Thread getProducer() {
		return producer;
	}
	
	public void init() {
		lastArrivingTime = 0;
		actualTime = 0;
		avgWaitingTime = 0;
		avgServiceTime = 0;
		emptyQueueTime = 0;
		peakHour = 0;
	}
	
	public void setSim(Simulation sim) {
		this.sim = sim;
	}
	
	public void start() {
		this.init();
		servers = new Service[this.nrServers];
		threads = new Thread[this.nrServers];
		for(int i = 0; i < this.nrServers; i++) {
			servers[i] = new Service("Server" + Integer.toString(i + 1), this, i);
			threads[i] = new Thread(servers[i]);
			threads[i].start();
		}
		gen = new ClientGenerator(this.nrServers, servers, this);
		producer = new Thread(gen);
		producer.start();

	}
	
}
