package GUI;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import WorkStation.WorkStation;
import WorkStation.ClientGenerator;
import WorkStation.Service;

public class Simulation implements ActionListener{
	
	boolean flag = false;
	int distBetween;
	WorkStation ws;
	JFrame appFrame;
	Container cPane;
	JLabel jlbSrv[] = new JLabel[5], time;
	JButton start, result ,clienti[] = new JButton[30];
	JTextArea textArea;
	JScrollPane scroll;
	PrintStream printStream;
	
	public Simulation(WorkStation ws) {
		this.ws = ws;
		prepareGUI();
	}
	
	private void prepareGUI() {
		appFrame = new JFrame("Simulation");
		appFrame.setSize(1200, 700);
		appFrame.setLocation(500, 150);
		appFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		appFrame.setResizable(false);
		cPane = appFrame.getContentPane();
		cPane.setLayout(null);
		
		arrangeComponents();
		appFrame.setVisible(true);
	}
	
	private void arrangeComponents() {
		
		// servers labels
		int i = ws.nrServers, index;
		distBetween = (700 - 100) / i;
		while (i != 0) {
			index = ws.nrServers - i;
			jlbSrv[index] = new JLabel("--Server " + Integer.toString(index + 1) + "--");
			jlbSrv[index].setFont(jlbSrv[index].getFont().deriveFont(17f));
			jlbSrv[index].setBounds(50 + index * distBetween, 100, 100, 80);
			jlbSrv[index].setBorder(BorderFactory.createLineBorder(Color.green));
			i--;
		}
		for (i = 0; i < ws.nrServers; i++) {
			cPane.add(jlbSrv[i]);
		}
		
		// time label
		time = new JLabel("00:00");
		time.setBorder(BorderFactory.createLineBorder(Color.black));
		time.setBounds(430, 10, 100, 65);                          
		time.setFont(time.getFont().deriveFont(35f));
		cPane.add(time);
		
		// start buttons
		start = new JButton("Start");
		start.setBounds(420, 580, 150, 80);
		start.setFont(start.getFont().deriveFont(40f));
		start.addActionListener(this);
		cPane.add(start);
		
		result = new JButton("Result");
		result.setBounds(800, 10, 100, 40);
		result.setFont(result.getFont().deriveFont(20f));
		result.addActionListener(this);
		cPane.add(result);
		
		textArea = new JTextArea();
		textArea.setEditable(false);
		printStream = new PrintStream(new CustomOutputStream(textArea));
		System.setOut(printStream);
		System.setErr(printStream); 
		scroll = new JScrollPane(textArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scroll.setBounds(700, 150, 490, 400);
		cPane.add(scroll);
	}
	
	 public void setTimp(int i) {
		int k1;
		k1 = i / 60;
		if (i - 60 * k1 < 10 && k1 < 10)
			time.setText("0" + k1 + ":0" + String.valueOf(i - 60 * k1));
		else if (i - 60 * k1 < 10)
			time.setText(k1 + ":0" + String.valueOf(i - 60 * k1));
		else if (i - 60 * k1 < 60 && k1 < 10)
			time.setText("0" + k1 + ":" + String.valueOf(i - 60 * k1));
		else if (i - 60 * k1 < 60)
			time.setText(k1 + ":" + String.valueOf(i - 60 * k1));
	}
	 
	 public void setEmpty(int i) {
			jlbSrv[i].setBorder(BorderFactory.createLineBorder(Color.orange));
	 }
	 
	 public void setBusy(int i) {
		 jlbSrv[i].setBorder(BorderFactory.createLineBorder(Color.red));
	 }
	 
	 public void setFree(int i) {
		 jlbSrv[i].setBorder(BorderFactory.createLineBorder(Color.green));
	 }
	 
	 public void createClient(int i, int ID, int lengCoada) {
		 clienti[ID] = new JButton("-" + Integer.toString(ID) + "-");
		 //clienti[ID].setBounds(jlbSrv[i].getBounds().x + 20, jlbSrv[i].getBounds().y + 150 + (lengCoada * 50), 40, 40);
		 clienti[ID].setBounds(jlbSrv[i].getBounds().x + 10, 200 + (50 * lengCoada), 50, 50);
		 clienti[ID].setFont(clienti[ID].getFont().deriveFont(30f));
		 clienti[ID].setBorder(BorderFactory.createLineBorder(Color.blue));
		 clienti[ID].setVisible(true);
		 cPane.add(clienti[ID]); 
		 cPane.revalidate();
		 cPane.repaint();
		 //cPane.validate();
	 }
	 
	 public void removeClient(int ID) {
		 clienti[ID].setVisible(false);
		 cPane.remove(clienti[ID]);
	 }

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == start) {
			textArea.setText("");
			flag = true;
			ws.setSim(this);
			ws.start();
		}
		if (flag && e.getSource() == result) {
			Service servers[] = ws.getServers();
			int nrClients = ClientGenerator.ID - 1;
			System.out.println("Total Waiting time: " + ws.avgWaitingTime + " Total service time: " + ws.avgServiceTime);
			System.out.println("Average Waiting time: " + ((float)ws.avgWaitingTime / nrClients) + " Average service time: " + ((float)ws.avgServiceTime / nrClients));
			for (int i = 0; i < ws.nrServers; i++) {
				System.out.println("Server " + Integer.toString(i + 1) + " | EmptyQueueTime: " + servers[i].getEmptyQueueTime());
				ws.emptyQueueTime += servers[i].getEmptyQueueTime();
			}
			System.out.println("Average Empty Queue Time : " + ((float)ws.emptyQueueTime / nrClients));
			System.out.println("Peak Hour : " + ws.peakHour);
			flag = false;
		}

	}
}

