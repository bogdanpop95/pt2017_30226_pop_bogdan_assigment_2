package GUI;
import WorkStation.WorkStation;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class Meniu implements ActionListener{
	
	JFrame appFrame;
	Container cPane;
	JLabel jlbServers, jlbMinArrivingTime, jlbMaxArrivingTime, jlbMinServiceTime, jlbMaxServiceTime, jlbSimulationInterval, jlbDescription;
	JTextField jtfServers, jtfMinArrivingTime, jtfMaxArrivingTime, jtfMinServiceTime, jtfMaxServiceTime, jtfSimulationInterval;
	JButton reset, start;
	JPanel descPanel;
	WorkStation ws;
	Simulation simulator;
	
	public Meniu() {
		prepareGUI();
	}
	
	private void prepareGUI() {
		appFrame = new JFrame("Meniu");
		appFrame.setSize(300, 550);
		appFrame.setLocation(700, 200);
		appFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		appFrame.setResizable(false);
		cPane = appFrame.getContentPane();
		cPane.setLayout(null);
		
		arrangeComponents();
		appFrame.setVisible(true);
	}
	
	private void arrangeComponents() {
		descPanel = new JPanel();
		descPanel.setBounds(20, 20, 250, 180);
		descPanel.setBorder(new TitledBorder(new EtchedBorder(), "Description"));
		
		jlbDescription = new JLabel("<html>Introduceti date valide!<br>Numbers of servers between 1-5<br>Minim ArrivingTime &lt Maxim ArrivingTime<br>Minim ServiceTime &lt Maxim ServiceTime<br>Simulation Interval &lt Maxim ArrivingTime<br><br>Go - Start Simulation<br>Reset - reset all data</html>", SwingConstants.CENTER);
		jlbDescription.setBounds(25, 25, 230, 160);
		descPanel.add(jlbDescription);
		
		cPane.add(descPanel);
		
		jlbServers = new JLabel("Numbers of servers:");
		jtfServers = new JTextField("");
		jlbServers.setBounds(60, 210, 120, 30);
		jtfServers.setBounds(180, 215, 30, 20);
		cPane.add(jlbServers);
		cPane.add(jtfServers);
		
		jlbMinArrivingTime = new JLabel("Minim ArrivingTime:");
		jtfMinArrivingTime = new JTextField("");
		jlbMinArrivingTime.setBounds(60, 250, 140, 30);
		jtfMinArrivingTime.setBounds(180, 255, 30, 20);
		cPane.add(jlbMinArrivingTime);
		cPane.add(jtfMinArrivingTime);
		
		jlbMaxArrivingTime = new JLabel("Maxim ArrivingTime:");
		jtfMaxArrivingTime = new JTextField("");
		jlbMaxArrivingTime.setBounds(60, 290, 140, 30);
		jtfMaxArrivingTime.setBounds(180, 295, 30, 20);
		cPane.add(jlbMaxArrivingTime);
		cPane.add(jtfMaxArrivingTime);
		
		jlbMinServiceTime = new JLabel("Minim ServiceTime:");
		jtfMinServiceTime = new JTextField("");
		jlbMinServiceTime.setBounds(60, 330, 140, 30);
		jtfMinServiceTime.setBounds(180, 335, 30, 20);
		cPane.add(jlbMinServiceTime);
		cPane.add(jtfMinServiceTime);
		
		jlbMaxServiceTime = new JLabel("Maxim ServiceTime:");
		jtfMaxServiceTime = new JTextField("");
		jlbMaxServiceTime.setBounds(60, 370, 140, 30);
		jtfMaxServiceTime.setBounds(180, 375, 30, 20);
		cPane.add(jlbMaxServiceTime);
		cPane.add(jtfMaxServiceTime);
		
		jlbSimulationInterval = new JLabel("Simulation Interval:");
		jtfSimulationInterval = new JTextField("");
		jlbSimulationInterval.setBounds(60, 410, 140, 30);
		jtfSimulationInterval.setBounds(180, 415, 30, 20);
		cPane.add(jlbSimulationInterval);
		cPane.add(jtfSimulationInterval);
		
		start = new JButton("GO");
		start.setBounds(120, 450, 60, 30);
		start.addActionListener(this);
		cPane.add(start);
		
		reset = new JButton("Reset");
		reset.setBounds(110, 490, 80, 25);
		reset.addActionListener(this);
		cPane.add(reset);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == start) {
			ws = new WorkStation(Integer.parseInt(jtfServers.getText()), Integer.parseInt(jtfMinArrivingTime.getText()), Integer.parseInt(jtfMaxArrivingTime.getText()), Integer.parseInt(jtfMinServiceTime.getText()), Integer.parseInt(jtfMaxServiceTime.getText()), Integer.parseInt(jtfSimulationInterval.getText()));
			appFrame.setVisible(false);
			simulator = new Simulation(ws);
		} else if (e.getSource() == reset) {
			jtfServers.setText(""); jtfMinArrivingTime.setText(""); jtfMaxArrivingTime.setText(""); jtfMaxServiceTime.setText(""); jtfMinServiceTime.setText(""); jtfSimulationInterval.setText("");
		}
	}
}
